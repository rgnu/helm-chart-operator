/**
 * 
 */
module.exports = class {
    constructor(type, status, lastTransitionTime=new Date()) {
        this.type = type
        this.status = status
        this.lastTransitionTime = lastTransitionTime
    }
}
