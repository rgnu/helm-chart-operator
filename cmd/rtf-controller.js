const _ = require('highland');
const Client = require('kubernetes-client').Client
const watchers = require('../watchers')
const handlers = require('../handlers')
const gatewayCRD = require('../resources/gateway-crd.json')

async function main() {
  const client = new Client({ version: '1.13' })

  client.addCustomResourceDefinition(gatewayCRD)

  const deploymentsWatcher = watchers.BaseWatcher(client.apis.apps.v1.watch.deployments)
  const configWatcher = watchers.BaseWatcher(client.api.v1.watch.namespace('rtf').configmaps)
  const secretsWatcher = watchers.BaseWatcher(client.api.v1.watch.namespace('rtf').secrets)
  const gatewaysWatcher = watchers.BaseWatcher(client.apis[gatewayCRD.spec.group][gatewayCRD.spec.version].watch.namespace('rtf').gateways)

  _([
    deploymentsWatcher.observe(),
    configWatcher.observe(),
    secretsWatcher,
    gatewaysWatcher,
  ])
  .merge()
  .each(handlers.LogHandler())

  //deploymentsWatcher
  //.errors(console.error)
  //.each(deployTrackerHandler())

  //helmchartsWatcher
  //.each(helmChartHandler(client))

  configWatcher
  .each(handlers.RtfEdgeTrackerHandler(client))
}

main().catch(console.error)