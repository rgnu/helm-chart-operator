const Client = require('kubernetes-client').Client
// const config = require('kubernetes-client').config
const client = new Client({ version: '1.11' })

/*
apiVersion: helm.cattle.io/v1
kind: HelmChart
metadata:
  name: rancher
  namespace: kube-system
spec:
  chart: rancher
  repo: https://releases.rancher.com/server-charts/stable/
  targetNamespace: cattle-system
  version: 2.2.3
  valuesContent: |
    hostname: "hostname"
    replicas: 1
    ingress:
      tls:
        source: rancher
*/

async function main() {
    const deploys = await client.apis.apps.v1.deployments.get()
    console.log('%j', deploys)
}


main().catch(console.error)