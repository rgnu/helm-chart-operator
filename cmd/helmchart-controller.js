const _ = require('highland');
const Client = require('kubernetes-client').Client
const watcher = require('../watchers/baseWatcher')
const logHandler = require('../handlers/logHandler')
const deployTrackerHandler = require('../handlers/deployTrackerHandler')
const helmChartHandler = require('../handlers/helmChartHandler')
const configMapTrackerHandler = require('../handlers/configMapTrackerHandler')
const crd = require('../resources/helmchart-crd.json')

async function main() {
  const client = new Client({ version: '1.13' })
  const { group, version } = crd.spec

  client.addCustomResourceDefinition(crd)

  const deploymentsWatcher = watcher(client.apis.apps.v1.watch.deployments)
  const configWatcher = watcher(client.api.v1.watch.configmaps)
  //const helmchartsWatcher = watcher(client.apis[group][version].watch.helmcharts)

  _([
    //helmchartsWatcher.observe(),
    deploymentsWatcher.observe(),
    configWatcher.observe(),
  ])
  .merge()
  .each(logHandler())

  //deploymentsWatcher
  //.errors(console.error)
  //.each(deployTrackerHandler())

  //helmchartsWatcher
  //.each(helmChartHandler(client))

  configWatcher
  .each(configMapTrackerHandler(client))
}

main().catch(console.error)