const BaseHandler = require('./BaseHandler')
const gatewayCRD = require('../resources/gateway-crd.json')

class RtfEdgeTrackerHandler extends BaseHandler {

    filter({type, object }) {
        const { labels } = object.metadata
        const id = `${object.metadata.namespace}/${object.kind}/${object.metadata.name}`

        if (!labels) return false
        if (labels['rtf.mulesoft.com/component'] !== 'agent') return false
        if (labels['rtf.mulesoft.com/type'] !== 'Edge') return false

        console.log(`${id}: Handling by RtfEdgeTrackerHandler`)

        return true
    }

    onAdded(object) {
        const cert = this.istioSecret(object)
        const gateway = this.istioGateway(object)

        return this.client.api.v1.namespaces(cert.metadata.namespace).secrets.post({ body : cert })
        .then(() => this.client.apis[gatewayCRD.spec.group][gatewayCRD.spec.version].namespaces(cert.metadata.namespace).gateways.post({ body: gateway}))
    }
    
    onModified(object) {
        const cert = this.istioSecret(object)
        const gateway = this.istioGateway(object)
    
        return this.client.api.v1.namespaces(cert.metadata.namespace).secret(cert.metadata.name).put({ body : cert })
        .catch()
        .then(() => this.client.apis[gatewayCRD.spec.group][gatewayCRD.spec.version].namespaces(cert.metadata.namespace).gateway(gateway.metadata.name).put({ body: gateway}))
    }

    istioSecret(object) {
        const config = Object.values(object.data).map(JSON.parse).reduce((p, c) => p = {...p, ...c}, {})

        return {
            apiVersion: 'v1',
            kind: 'Secret',
            metadata: {
                namespace: object.metadata.namespace,
                name: `${object.metadata.name}-certs`,
                labels: object.metadata.labels,
                annotations: {},
                ownerReferences: [{
                    apiVersion: object.apiVersion,
                    controller: true,
                    blockOwnerDeletion: true,
                    kind: object.kind,
                    name: object.metadata.name,
                    uid: object.metadata.uid,
                }]
            },
            stringData: {
                key: config.keystore.key,
                cert: config.keystore.certificate
            }
        }
    }

    istioGateway(object) {
        const config = Object.values(object.data).map(JSON.parse).reduce((p, c) => p = {...p, ...c}, {})
        const host = config.edgeConfiguration.virtualServers[0].virtualServer.cn
        const portHttp = config.edgeConfiguration.portInfo.redirectFrom
        const portHttps = config.edgeConfiguration.portInfo.number
    
        return {
            apiVersion: 'networking.istio.io/v1alpha3',
            kind: 'Gateway',
            metadata: {
                namespace: object.metadata.namespace,
                name: `${object.metadata.name}`,
                labels: object.metadata.labels,
                annotations: {},
                ownerReferences: [{
                    apiVersion: object.apiVersion,
                    controller: true,
                    blockOwnerDeletion: true,
                    kind: object.kind,
                    name: object.metadata.name,
                    uid: object.metadata.uid,
                }]
            },
            spec: {
                selector: { istio: 'ingressgateway' },
                servers: [
                    {
                        port: {
                            number: portHttp,
                            name: 'http',
                            protocol: 'HTTP'
                        },
                        hosts: [host]
                    },
                    {
                        port: {
                            number: portHttps,
                            name: 'https',
                            protocol: 'HTTPS'
                        },
                        tls: {
                            mode: 'SIMPLE',
                            credentialName: `${object.metadata.name}-certs`,
                            httpsRedirect: true
                        },
                        hosts: [host]
                    }
                ]
            }
        }
    }
}

/**
 * 
 * 
 */
module.exports = (client) => {
    const handler = new RtfEdgeTrackerHandler(client)

    return handler.handle.bind(handler)
}