const crd = require('../resources/helmchart-crd.json')
const Condition = require('../models/Condition')

/**
 * 
 * @param type The action taked
 * @param object Object
 */
module.exports = (client) => {
    const { group, version } = crd.spec

    return ({ type, object }) => {
        const {namespace, name} = object.metadata
        console.log(`${namespace}/${name}: ${type} (${object.metadata.uid}/${object.metadata.resourceVersion})`)

        if (['ADDED'].includes(type)) {
            object.status = { conditions: [new Condition("Ready", "True")] }
            client.apis[group][version].namespace(namespace).helmchart(name).status.put({ body : object }).catch(console.error)
        }
    }
}
