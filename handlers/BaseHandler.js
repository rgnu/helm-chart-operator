/**
 * 
 */
module.exports = class {
    constructor(client) {
        this.client = client
    }

    handle({ type, object }) {
        if (!this.filter({ type, object })) return Promise.resolve()

        if (type==='ADDED') return Promise.resolve(this.onAdded(object))
        if (type==='MODIFIED') return Promise.resolve(this.onModified(object))
        if (type==='DELETED') return Promise.resolve(this.onDeleted(object))

        return Promise.resolve()
    }
    
    filter({ type, object }) {
        return true
    }

    onAdded(object) {}

    onModified(object) {}

    onDeleted(object) {}
}