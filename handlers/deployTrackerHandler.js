/**
 * 
 * @param action The action taked
 * @param object Object
 */
module.exports = () => {
    const versions = {}

    return ({ action, object }) => {
        const id = `${object.metadata.namespace}/${object.metadata.name}`
        const { replicas=0, readyReplicas=0} = object.status
        const newVersion = object.spec.template.spec.containers.map(container => container.image).join(',')
        const version = versions[id] || '(none)'

        console.log(`${id}: ${version} -> ${newVersion} (${readyReplicas}/${replicas})`)

        if (version !== newVersion) {
            versions[id] = newVersion
        }
    }
}
