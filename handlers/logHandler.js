/**
 * 
 * @param type The action taked
 * @param object Object
 */
module.exports = () => ({ type, object }) => {
    const id = `${object.metadata.namespace}/${object.kind}/${object.metadata.name}`
    console.log(`${id}: ${type} (${object.metadata.uid}/${object.metadata.resourceVersion})`)
}
