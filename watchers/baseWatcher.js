const _ = require('highland');

module.exports = (api) => {
  let resourceVersion = null
  const stream = _((pull, next) => {
    pull(null, api.getStream({ qs: { resourceVersion } }))
    next()
  })

  return stream
    .map(_)
    .sequence()
    .split()
    .map(JSON.parse)
    .errors(console.error)
    .tap(event => {
      resourceVersion = event.object.metadata.resourceVersion
    })
}